import env from '../.env'
import Telegraf, {Extra, Markup, session} from 'telegraf'

const bot = new Telegraf(env.token)

const bottons = lista  => Extra.markup(
    Markup.inlineKeyboard(
        lista.map(item => Markup.callbackButton(item, `delete ${item}`)),
        {columns: 3}
    )
)

bot.use(session())

bot.start(async ctx => {
    const name = ctx.update.message.from.first_name
    await ctx.reply(`Seja bem vindo, ${name}!`)
    await ctx.reply(`Escreva os itens que você deseja adicionar...`)
    ctx.session.lista = []
})

bot.on('text', ctx => {
    let message = ctx.update.message.text 
    
    ctx.reply(`${message} adicionado!`, bottons(ctx.session.lista))
})

// Para excluir um item
bot.action(/delete (.+)/, ctx => {
    ctx.session.lista = ctx.session.lista.filter(item => item != ctx.match[1])
    ctx.reply(`${ctx.match[1]} deletado!`, bottons(ctx.session.lista))
})

bot.startPolling()