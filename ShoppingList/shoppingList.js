import env from '../.env'
import Telegraf, {Extra, Markup} from 'telegraf'
import { button } from 'telegraf/markup'

const bot = new Telegraf(env.token)

let list = []

const bottons = () => Extra.markup(
    Markup.inlineKeyboard(
        list.map(item => Markup.callbackButton(item, `delete ${item}`)),
        {columns: 3}
    )
)

bot.start(async ctx => {
    const name = ctx.update.message.from.first_name
    await ctx.reply(`Seja bem vindo, ${name}!`)
    await ctx.reply(`Escreva os itens que você deseja adicionar...`)
})

bot.on('text', ctx => {
    const message = ctx.update.message.text 
    list.push(message)
    ctx.reply(`${message} adicionado!`, bottons())
})


bot.action(/delete (.+)/, ctx => {
    list= list.filter(item => item != ctx.match[1])
    ctx.reply(`${ctx.match[1]} deletado!`, bottons())
})

bot.startPolling()