const env = require('../.env')
const Telegraf = require('telegraf')
const bot = new Telegraf(env.token)

bot.start(async ctx => {
    const from = ctx.update.message.from
    await ctx.reply(`Seja bem vindo, ${from.first_name} `)
    await ctx.replyWithHTML(`
    Destacando mensagem <b> HTML </b>
    <i> de várias </i> <code> formas </code> <pre> possíveis </pre>
    <a href="http://www.google.com.br"> Google </a>
    `)

    await ctx.replyWithMarkdown(`'*Olá meus amigos!*'`)


    // await ctx.replyWithPhoto({source: `${__dirname}/have.jpg`})
     await ctx.replyWithPhoto('https://www.macleans.ca/wp-content/uploads/2018/01/JAN22_BETHUNE_MARY_POST01.jpg', 
     {caption: `Linda demais`})

     await ctx.replyWithLocation(-11.178402,-52.511294)

    // await ctx.replyWithVideo('https://www.youtube.com/watch?v=1gFsAMVLVIs&ab&ab')
    
})

bot.startPolling()      